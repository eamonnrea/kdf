# Translation of kdf.po to Brazilian Portuguese
# Copyright (C) 2000, 2003, 2005, 2008, 2009, 2012, 2016 Free Software Foundation, Inc.
#
# Raul Fernandes <rgfernandes@yahoo.com>, 2000.
# Antonio Sergio de Mello e Souza <asergioz@bol.com.br>, 2003.
# Henrique Pinto <henrique.pinto@kdemail.net>, 2005.
# André Marcelo Alvarenga <alvarenga@kde.org>, 2008, 2009, 2012, 2016.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2009, 2019, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdf\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-27 00:52+0000\n"
"PO-Revision-Date: 2022-02-02 08:49-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""
"Rodrigo Stulzer, Antonio Sergio de Mello e Souza, André Marcelo Alvarenga"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "rodrigo@conectiva.com.br, asergioz@bol.com.br, alvarenga@kde.org"

#. i18n: ectx: Menu (file)
#: desktop/kdfui.rc:5
#, kde-format
msgid "&File"
msgstr "&Arquivo"

#: src/disklist.cpp:314
#, kde-format
msgid "could not execute [%1]"
msgstr "não foi possível executar [%1]"

#: src/disks.cpp:262
#, kde-format
msgid ""
"Called: %1\n"
"\n"
msgstr ""
"Chamada: %1\n"
"\n"

#: src/disks.cpp:283
#, kde-format
msgid "could not execute %1"
msgstr "não foi possível executar %1"

#: src/kcmdf.cpp:49
#, kde-format
msgid ""
"A right mouse button click opens a context menu to mount/unmount a device or "
"to open it in the file manager."
msgstr ""
"Um clique com o botão direito do mouse abre um menu de contexto para montar/"
"desmontar um dispositivo ou para o abri-lo no gerenciador de arquivos."

#: src/kdf.cpp:26
#, kde-format
msgctxt "Update action"
msgid "&Update"
msgstr "At&ualizar"

#: src/kdf.cpp:59
#, kde-format
msgid "KDiskFree"
msgstr "KDiskFree"

#: src/kdf.cpp:61
#, kde-format
msgid "KDE free disk space utility"
msgstr "Utilitário de controle de espaço livre em disco do KDE"

#: src/kdf.cpp:63
#, kde-format
msgid "(c) 1998-2001, Michael Kropfberger"
msgstr "(c) 1998-2001, Michael Kropfberger"

#: src/kdf.cpp:69 src/kwikdisk.cpp:318
#, kde-format
msgid "Michael Kropfberger"
msgstr "Michael Kropfberger"

#: src/kdfconfig.cpp:32
#, kde-format
msgctxt "TODO"
msgid "Icon"
msgstr "Ícone"

#: src/kdfconfig.cpp:33 src/kdfwidget.cpp:55
#, kde-format
msgctxt "Device of the storage"
msgid "Device"
msgstr "Dispositivo"

#: src/kdfconfig.cpp:34 src/kdfwidget.cpp:56
#, kde-format
msgctxt "Filesystem on storage"
msgid "Type"
msgstr "Tipo"

#: src/kdfconfig.cpp:35 src/kdfwidget.cpp:57
#, kde-format
msgctxt "Total size of the storage"
msgid "Size"
msgstr "Tamanho"

#: src/kdfconfig.cpp:36
#, kde-format
msgctxt "Mount point of the storage"
msgid "Mount Point"
msgstr "Ponto de montagem"

#: src/kdfconfig.cpp:37 src/kdfwidget.cpp:59
#, kde-format
msgctxt "Free space in storage"
msgid "Free"
msgstr "Livre"

#: src/kdfconfig.cpp:38 src/kdfwidget.cpp:60
#, kde-format
msgctxt "Used storage space in %"
msgid "Full %"
msgstr "% usado"

#: src/kdfconfig.cpp:39 src/kdfwidget.cpp:61
#, kde-format
msgctxt "Usage graphical bar"
msgid "Usage"
msgstr "Uso"

#: src/kdfconfig.cpp:64 src/kdfconfig.cpp:184
#, kde-format
msgctxt "Visible items on device information columns (enable|disable)"
msgid "visible"
msgstr "visível"

#: src/kdfconfig.cpp:163
#, kde-format
msgctxt "Are items on device information columns visible?"
msgid "visible"
msgstr "visível"

#: src/kdfconfig.cpp:164
#, kde-format
msgctxt "Are items on device information columns hidden?"
msgid "hidden"
msgstr "oculto"

#: src/kdfconfig.cpp:201
#, kde-format
msgctxt "Device information item is hidden"
msgid "hidden"
msgstr "oculto"

#: src/kdfconfig.cpp:202
#, kde-format
msgctxt "Device information item is visible"
msgid "visible"
msgstr "visível"

#. i18n: ectx: property (text), widget (QLabel, updateLabel)
#: src/kdfconfig.ui:35
#, kde-format
msgid "Update frequency:"
msgstr "Frequência de atualização:"

#. i18n: ectx: property (suffix), widget (QSpinBox, m_updateSpinBox)
#: src/kdfconfig.ui:42
#, kde-format
msgid " sec"
msgstr " s"

#. i18n: ectx: property (text), widget (QCheckBox, mOpenMountCheck)
#: src/kdfconfig.ui:55
#, kde-format
msgid "Open file manager automatically on mount"
msgstr "Abrir o gerenciador de arquivos automaticamente na montagem"

#. i18n: ectx: property (text), widget (QCheckBox, mPopupFullCheck)
#: src/kdfconfig.ui:62
#, kde-format
msgid "Pop up a window when a disk gets critically full"
msgstr "Exibir uma janela quando um disco tiver um nível de utilização crítico"

#. i18n: ectx: property (text), widget (QCheckBox, mSystemFileManagerCheck)
#: src/kdfconfig.ui:69
#, kde-format
msgid "Use system default file manager"
msgstr "Usar gerenciador de arquivos padrão do sistema"

#. i18n: ectx: property (text), widget (QLabel, fileManagerEdit)
#: src/kdfconfig.ui:76
#, no-c-format, kde-format
msgid "File manager (e.g. konsole -e mc %m):"
msgstr "Gerenciador de arquivos (ex.: konsole -e mc %m):"

#. i18n: ectx: property (text), widget (QLabel, mFlatpakLabel)
#: src/kdfconfig.ui:94
#, kde-format
msgid "Custom file manager not supported when running as Flatpak."
msgstr ""
"Gerenciador de arquivos personalizado não suportado quando executado como "
"Flatpak."

#: src/kdfitemdelegate.cpp:32 src/kdfwidget.cpp:292
#, kde-format
msgctxt "Disk percentage"
msgid "%1%"
msgstr "%1%"

#: src/kdfwidget.cpp:58
#, kde-format
msgctxt "Mount point of storage"
msgid "Mount Point"
msgstr "Ponto de montagem"

#: src/kdfwidget.cpp:297 src/kdfwidget.cpp:298
#, kde-format
msgid "N/A"
msgstr "N/D"

#: src/kdfwidget.cpp:355 src/kwikdisk.cpp:267
#, kde-format
msgid "Device [%1] on [%2] is critically full."
msgstr ""
"O dispositivo [%1] em [%2] está ficando com um nível de utilização crítico."

#: src/kdfwidget.cpp:357
#, kde-format
msgctxt "Warning device getting critically full"
msgid "Warning"
msgstr "Aviso"

#: src/kdfwidget.cpp:405
#, kde-format
msgid "Mount Device"
msgstr "Montar dispositivo"

#: src/kdfwidget.cpp:406
#, kde-format
msgid "Unmount Device"
msgstr "Desmontar dispositivo"

#: src/kdfwidget.cpp:408
#, kde-format
msgid "Open in File Manager"
msgstr "Abrir no gerenciador de arquivos"

#: src/kdfwidget.cpp:425 src/kdfwidget.cpp:428
#, kde-format
msgid "MOUNTING"
msgstr "MONTANDO"

#: src/kwikdisk.cpp:60 src/kwikdisk.cpp:309
#, kde-format
msgid "KwikDisk"
msgstr "KwikDisk"

#: src/kwikdisk.cpp:66
#, kde-format
msgid "&Start KDiskFree"
msgstr "&Iniciar o KDiskFree"

#: src/kwikdisk.cpp:70
#, kde-format
msgid "&Configure KwikDisk..."
msgstr "&Configurar o KwikDisk..."

#: src/kwikdisk.cpp:178
#, kde-format
msgctxt "Unmount the storage device"
msgid "Unmount"
msgstr "Desmontar"

#: src/kwikdisk.cpp:178
#, kde-format
msgctxt "Mount the storage device"
msgid "Mount"
msgstr "Montar"

#: src/kwikdisk.cpp:201
#, kde-format
msgid "You must login as root to mount this disk"
msgstr "Você precisa ser superusuário para montar este disco"

#: src/kwikdisk.cpp:269
#, kde-format
msgctxt "Device is getting critically full"
msgid "Warning"
msgstr "Aviso"

#: src/kwikdisk.cpp:311
#, kde-format
msgid "KDE Free disk space utility"
msgstr "Utilitário de controle de espaço livre em disco do KDE"

#: src/kwikdisk.cpp:313
#, kde-format
msgid "(C) 2004 Stanislav Karchebny"
msgstr "(C) 2004 Stanislav Karchebny"

#: src/kwikdisk.cpp:319
#, kde-format
msgid "Original author"
msgstr "Autor original"

#: src/kwikdisk.cpp:321
#, kde-format
msgid "Espen Sand"
msgstr "Espen Sand"

#: src/kwikdisk.cpp:322
#, kde-format
msgid "KDE 2 changes"
msgstr "Alterações para o KDE 2"

#: src/kwikdisk.cpp:323
#, kde-format
msgid "Stanislav Karchebny"
msgstr "Stanislav Karchebny"

#: src/kwikdisk.cpp:324
#, kde-format
msgid "KDE 3 changes"
msgstr "Alterações para o KDE 3"

#: src/mntconfig.cpp:47 src/mntconfig.cpp:52
#, kde-format
msgid "Device"
msgstr "Dispositivo"

#: src/mntconfig.cpp:48 src/mntconfig.cpp:54
#, kde-format
msgid "Mount Point"
msgstr "Ponto de montagem"

#: src/mntconfig.cpp:48
#, kde-format
msgid "Mount Command"
msgstr "Comando de montagem"

#: src/mntconfig.cpp:48
#, kde-format
msgid "Unmount Command"
msgstr "Comando de desmontagem"

#: src/mntconfig.cpp:53
#, kde-format
msgctxt "No device is selected"
msgid "None"
msgstr "Nenhum"

#: src/mntconfig.cpp:55
#, kde-format
msgctxt "No mount point is selected"
msgid "None"
msgstr "Nenhum"

#: src/mntconfig.cpp:244
#, kde-format
msgid "Only local files supported."
msgstr "Somente arquivos locais são suportados."

#: src/mntconfig.cpp:260
#, kde-format
msgid "Only local files are currently supported."
msgstr "Somente arquivos locais são suportados."

#. i18n: ectx: property (text), widget (QLabel, iconNameLabel)
#: src/mntconfig.ui:20
#, kde-format
msgid "Icon name:"
msgstr "Nome do ícone:"

#. i18n: ectx: property (text), widget (QLabel, mountLabel)
#: src/mntconfig.ui:30
#, kde-format
msgid "Mount Command:"
msgstr "Comando de montagem:"

#. i18n: ectx: property (text), widget (QLabel, umountLabel)
#: src/mntconfig.ui:40
#, kde-format
msgid "Unmount Command:"
msgstr "Comando de desmontagem:"

#. i18n: ectx: property (text), widget (QPushButton, mDefaultIconButton)
#: src/mntconfig.ui:50
#, kde-format
msgid "Default Icon"
msgstr "Ícone padrão"

#. i18n: ectx: property (text), widget (QPushButton, mMountButton)
#. i18n: ectx: property (text), widget (QPushButton, mUmountButton)
#: src/mntconfig.ui:57 src/mntconfig.ui:64
#, kde-format
msgid "Get Command..."
msgstr "Obter comando..."

#: src/optiondialog.cpp:18
#, kde-format
msgid "Configure"
msgstr "Configurar"

#: src/optiondialog.cpp:24
#, kde-format
msgid "General Settings"
msgstr "Configurações gerais"

#: src/optiondialog.cpp:28
#, kde-format
msgid "Mount Commands"
msgstr "Comandos de montagem"
